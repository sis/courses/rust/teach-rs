# Unit 6.2 - Full Stack web Apps

## Exercise 6.2.1: Full Stack web App using dioxus

This exercise is about writing a full stack web application.

We refer to the [Rust Full Stack
Workshop](https://bcnrust.github.io/devbcn-workshop/#building-a-movie-collection-manager---full-stack-workshop-with-rust-actix-sqlx-dioxus-and-shuttle).

Be aware that the API of [dioxus](https://dioxuslabs.com/) changed
slightly in the mean time.
