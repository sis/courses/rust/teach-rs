# Summary

- [Course Introduction]()
	- [Introduction](introduction.md)

- [Foundations of Rust]()
	- [Basic Syntax](basic-syntax.md)
	- [Ownership and References](ownership-and-references.md)
	- [Advanced Syntax](advanced-syntax.md)
	- [Traits and Generics](traits-and-generics.md)
	- [Closures and Dynamic dispatch](closures-and-dynamic-dispatch.md)
	- [Interior mutability](interior-mutability.md)

- [Multitasking]()
	- [Introduction to Multitasking](introduction-to-multitasking.md)
	- [Parallel Multitasking](parallel-multitasking.md)
	- [Asynchronous Multitasking](asynchronous-multitasking.md)

- [Scientific Computing with Rust]()
	- [Scientific Computing with Rust](scientific-computing-with-rust.md)

- [Rust for Web]()
	- [Rust for Web](rust-for-web.md)

- [Rust and wasm]()
	- [Embedded wasm apps](embedded-wasm-apps.md)
	- [Full Stack web Apps](full-stack-web-apps.md)
	- [wasm containers](wasm-containers.md)

- [Command line interfaces]()
	- [CLI](cli.md)

