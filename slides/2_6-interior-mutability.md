---
theme: teach-rs
class: text-center
highlighter: shiki
lineNumbers: true
info: "Rust - 2.6: Interior mutability"
drawings:
    persist: false
fonts:
    mono: Fira Mono
layout: cover
title: "Rust - 2.6: Interior mutability"
---

# Rust programming

Module 2: Foundations of Rust

## Unit 6

Interior mutability

---

# Learning objectives



---
layout: section
---

# Interior mutability

---

# To do:

Issue: [tweedegolf/teach-rs#67](https://github.com/tweedegolf/teach-rs/issues/67)


---

# Summary
