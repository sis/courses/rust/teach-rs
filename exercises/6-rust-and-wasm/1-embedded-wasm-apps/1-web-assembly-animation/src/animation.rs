/// Moving 2D ball
pub struct Ball {
    pub pos: [f64; 2],
    /// Velocity
    pub v: [f64; 2],
    /// Radius
    pub r: f64,
}

/// 2D rectangle
pub struct Rectangle {
    pub x: f64,
    pub y: f64,
    pub width: f64,
    pub height: f64,
}

/// State of the animation
pub struct Animation {
    pub boundary: Rectangle,
    pub balls: Vec<Ball>,
}

/// Orientation of a collision.
enum Orientation {
    X,
    Y,
}

/// Time until x + v*t hits 0 or width.
fn t_collision_1d(x: f64, v: f64, width: f64, r: f64) -> f64 {
    if v > 0. {
        // Case: x + t*v = width - r
        (width - x - r) / v
    } else {
        // Case: x + t*v = r
        (r - x) / v
    }
}

/// Time until a collision of a ball with a containing rectangle.
fn t_collision(ball: &Ball, rectangle: &Rectangle) -> (f64, Orientation) {
    let t_x = t_collision_1d(
        ball.pos[0] - rectangle.x,
        ball.v[0],
        rectangle.width,
        ball.r,
    );
    let t_y = t_collision_1d(
        ball.pos[1] - rectangle.y,
        ball.v[1],
        rectangle.height,
        ball.r,
    );
    match (t_x >= 0., t_y >= 0.) {
        (true, true) => {
            if t_x < t_y {
                (t_x, Orientation::X)
            } else {
                (t_y, Orientation::Y)
            }
        }
        (true, false) => (t_x, Orientation::X),
        (false, true) => (t_y, Orientation::Y),
        // Orientation does not matter, t < 0 / no collision:
        (false, false) => (f64::max(t_x, t_y), Orientation::Y),
    }
}

impl Animation {
    /// Evolve the simulation by `dt`
    pub fn step(&mut self, dt: f64) {
        let mut t_remaining = dt;
        while t_remaining > 0. {
            // Determine time until next collision:
            if let Some((ball_index, (dt, orientation))) = self
                .balls
                .iter()
                .enumerate()
                .map(|(ball_index, ball)| (ball_index, t_collision(ball, &self.boundary)))
                .filter(|(_, (t, _))| 0. <= *t && *t < t_remaining)
                .min_by(|(_, (t1, _)), (_, (t2, _))| {
                    t1.partial_cmp(t2).unwrap_or(std::cmp::Ordering::Equal)
                })
            {
                self.substep(dt);
                t_remaining -= dt;
                let dim = match orientation {
                    Orientation::X => 0,
                    Orientation::Y => 1,
                };
                let ball = &mut self.balls[ball_index];
                ball.v[dim] = -ball.v[dim];
            } else {
                self.substep(t_remaining);
                break;
            }
        }
    }

    /// Evolve the simulation by `dt` without collisions
    pub fn substep(&mut self, dt: f64) {
        for ball in &mut self.balls {
            ball.pos[0] += ball.v[0] * dt;
            ball.pos[1] += ball.v[1] * dt;
        }
    }
}
