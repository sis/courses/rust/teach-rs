mod animation;
mod graphics;
mod ui;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const WIDTH: f64 = 400.;
    const HEIGHT: f64 = 400.;

    let animation = crate::animation::Animation::new(WIDTH, HEIGHT, 20);
    crate::ui::run(animation, false)?;
    let animation =
        crate::animation::Animation::load_from_json(&std::path::PathBuf::from("gumpiball.json"))?;
    let trajectory = crate::animation::Trajectory::track(animation, 0., 100., 10.);
    crate::graphics::svg::save_to_svg(&trajectory, &std::path::PathBuf::from("trajectory.svg"))?;
    Ok(())
}
