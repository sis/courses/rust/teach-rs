use crate::animation;
use crate::graphics::Render;

/// Run simulation in a window.
///
/// Keyboard:
/// Esc/Q: exit
/// ↑/↓: increase/decrease the time step
/// P: pause
pub fn run(
    animation: animation::Animation,
    paused: bool,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut app = App::new(animation, paused);
    let boundary = &app.animation.boundary;
    let mut window = minifb::Window::new(
        "Gumpiball",
        boundary.width as usize,
        boundary.height as usize,
        minifb::WindowOptions {
            ..minifb::WindowOptions::default()
        },
    )?;
    // Limit to max update rate
    const DT: u64 = 16600; // ~60 fps
    window.set_target_fps(1_000_000 / DT as usize);
    let (w, h) = window.get_size();
    let mut canvas = crate::graphics::raqote::CanvasRaqote::new(w as f64, h as f64);
    while window.is_open()
        && !window.is_key_down(minifb::Key::Escape)
        && !window.is_key_down(minifb::Key::Q)
    {
        if window.is_key_pressed(minifb::Key::Space, minifb::KeyRepeat::No) {
            app.paused = !app.paused;
        }
        if window.is_key_pressed(minifb::Key::Up, minifb::KeyRepeat::No) {
            app.increase_framerate();
        }
        if window.is_key_pressed(minifb::Key::Down, minifb::KeyRepeat::No) {
            app.decrease_framerate();
        }
        if !app.paused {
            app.step();
            app.animation.render(&mut canvas);

            window.update_with_buffer(canvas.get_data(), w, h)?;
        } else {
            window.update();
        }
        std::thread::sleep(std::time::Duration::from_micros(DT));
    }
    Ok(())
}

/// Application state
struct App {
    animation: animation::Animation,
    pub paused: bool,
    /// Time step
    pub dt: f64,
}

impl App {
    fn new(animation: animation::Animation, paused: bool) -> Self {
        App {
            animation,
            paused,
            dt: 1.0,
        }
    }
    fn increase_framerate(&mut self) {
        self.dt *= 2.;
    }
    fn decrease_framerate(&mut self) {
        self.dt /= 2.;
    }
    fn step(&mut self) {
        self.animation.step(self.dt);
    }
}
