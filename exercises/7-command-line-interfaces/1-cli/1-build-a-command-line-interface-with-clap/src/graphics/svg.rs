//! svg graphics backend

use crate::graphics::{self, BoundingBox, FillCircle, FillRectangle, Render, StrokeRectangle};
use svg::node::{
    element::{Circle, Rectangle},
    Value,
};

/// Save an object which implements [Render] and [BoundingBox] to an svg file
pub fn save_to_svg<O: Render + BoundingBox>(
    object: &O,
    path: &std::path::Path,
) -> Result<(), std::io::Error> {
    let boundary = object.bounding_box();
    let mut svg = CanvasSvg::new(boundary.x, boundary.y, boundary.width, boundary.height);
    object.render(&mut svg);
    svg.save(path)
}

/// svg canvas implementing [crate::graphics::Canvas]
pub struct CanvasSvg {
    doc: svg::Document,
}

impl Default for CanvasSvg {
    fn default() -> Self {
        Self {
            doc: svg::Document::new(),
        }
    }
}

impl StrokeRectangle for CanvasSvg {
    fn stroke_rectangle(
        &mut self,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        line_style: &graphics::LineStyle,
    ) {
        self.add(rect_path(x, y, w, h).set_stroke(line_style));
    }
}

impl FillRectangle for CanvasSvg {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &graphics::Color) {
        self.add(rect_path(x, y, w, h).set_fill(color));
    }
}

impl FillCircle for CanvasSvg {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &graphics::Color) {
        self.add(circle_path(x, y, r).set_fill(color));
    }
}

impl CanvasSvg {
    /// Create a new svg canvas for a given bounding box
    pub fn new(x: f64, y: f64, w: f64, h: f64) -> Self {
        Self {
            doc: svg::Document::new().set("viewBox", (x, y, w, h)),
        }
    }
    /// Save the canvas to an svg file
    pub fn save<T: AsRef<std::path::Path>>(&self, filename: T) -> Result<(), std::io::Error> {
        svg::save(filename, &self.doc)
    }
    /// Add a svg element to the canvas
    fn add<T: svg::node::Node>(&mut self, node: T) {
        self.doc = std::mem::take(self).doc.add(node);
    }
}

/// Helper trait to shorten expressions when building svg elements
trait ModifyNode: Sized {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self;
    fn set_stroke(self, line_style: &graphics::LineStyle) -> Self {
        let obj = self
            .set("stroke-width", line_style.width)
            .set("fill", "none")
            .set("stroke", to_svg_color(&line_style.color));
        if line_style.color.a == 255 {
            obj
        } else {
            obj.set("stroke-opacity", line_style.color.a as f64 / 255.)
        }
    }
    fn set_fill(self, color: &graphics::Color) -> Self {
        let obj = self.set("fill", to_svg_color(color));
        if color.a == 255 {
            obj
        } else {
            obj.set("fill-opacity", color.a as f64 / 255.)
        }
    }
}

impl ModifyNode for Rectangle {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self {
        self.set(name, value)
    }
}
impl ModifyNode for svg::node::element::Path {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self {
        self.set(name, value)
    }
}
impl ModifyNode for Circle {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self {
        self.set(name, value)
    }
}

fn rect_path(x: f64, y: f64, w: f64, h: f64) -> Rectangle {
    Rectangle::new()
        .set("x", x)
        .set("y", y)
        .set("width", w)
        .set("height", h)
}

fn circle_path(x: f64, y: f64, r: f64) -> Circle {
    Circle::new().set("cx", x).set("cy", y).set("r", r)
}

fn to_svg_color(clr: &graphics::Color) -> String {
    format!("#{:02X}{:02X}{:02X}", clr.r, clr.g, clr.b)
}
