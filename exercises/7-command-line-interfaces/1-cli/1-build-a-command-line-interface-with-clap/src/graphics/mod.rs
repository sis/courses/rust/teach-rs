//! Abstraction traits for graphics backends (svg/ui) and graphics structs

pub mod raqote;
pub mod svg;

/// Abstraction of stroking a rectangle
pub trait StrokeRectangle {
    fn stroke_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, line_style: &LineStyle);
}

/// Abstraction of filling a rectangle
pub trait FillRectangle {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &Color);
}

/// Abstraction of filling a circle
pub trait FillCircle {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &Color);
}

/// Abstraction of all graphical instructions needed for the animation
pub trait Canvas: StrokeRectangle + FillRectangle + FillCircle {}

impl<T: StrokeRectangle + FillRectangle + FillCircle> Canvas for T {}

/// Abstraction of rendering an object to a canvas using the above graphics traits
pub trait Render {
    fn render(&self, canvas: &mut impl Canvas);
}

/// Determine a bounding box of an object
///
/// This is needed when rendering to a svg canvas
pub trait BoundingBox {
    fn bounding_box(&self) -> crate::animation::Rectangle;
}

/// Representation of a web color
#[derive(Debug, Clone, Default, serde::Deserialize)]
#[serde(try_from = "String")]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl std::str::FromStr for Color {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let bytes = s
            .get(..1)
            .filter(|head| head == &"#" && (s.len() == 7 || s.len() == 9))
            .and(s.get(1..))
            .and_then(|tail| u32::from_str_radix(tail, 16).ok())
            .map(|b| if s.len() == 7 { (b << 8) + 0xFF } else { b })
            .ok_or(format!("Invalid color: {}", s))?;
        let [r, g, b, a] = bytes.to_be_bytes();
        Ok(Color { r, g, b, a })
    }
}

// Need this for json deserialization
impl TryFrom<String> for Color {
    type Error = <Self as std::str::FromStr>::Err;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        <Self as std::str::FromStr>::from_str(&s)
    }
}

impl Color {
    pub const WHITE: Self = Self {
        r: 255,
        g: 255,
        b: 255,
        a: 255,
    };
    pub const BLACK: Self = Self {
        r: 0,
        g: 0,
        b: 0,
        a: 255,
    };
}

/// Representation of a line style
pub struct LineStyle {
    pub width: f64,
    pub color: Color,
}

impl LineStyle {
    pub const DEFAULT_LINE_STYLE: Self = Self {
        width: 1.,
        color: Color::BLACK,
    };
}
