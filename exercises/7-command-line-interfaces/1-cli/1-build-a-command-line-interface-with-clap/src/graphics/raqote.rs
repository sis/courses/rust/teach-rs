//! ui graphics backend

use crate::graphics::{self, FillCircle, FillRectangle, StrokeRectangle};

/// ui canvas implementing [crate::graphics::Canvas]
pub struct CanvasRaqote(raqote::DrawTarget);

impl CanvasRaqote {
    pub fn new(w: f64, h: f64) -> Self {
        Self(raqote::DrawTarget::new(w as i32, h as i32))
    }
    pub fn get_data(&self) -> &[u32] {
        self.0.get_data()
    }
}

impl StrokeRectangle for CanvasRaqote {
    fn stroke_rectangle(
        &mut self,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        line_style: &graphics::LineStyle,
    ) {
        let mut pb = raqote::PathBuilder::new();
        pb.rect(x as f32, y as f32, w as f32, h as f32);
        self.0.stroke(
            &pb.finish(),
            &raqote::Source::Solid(into_solid_source(&line_style.color)),
            &into_stroke_style(line_style),
            &raqote::DrawOptions::new(),
        );
    }
}

impl FillRectangle for CanvasRaqote {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &graphics::Color) {
        self.0.fill_rect(
            x as f32,
            y as f32,
            w as f32,
            h as f32,
            &raqote::Source::Solid(into_solid_source(color)),
            &raqote::DrawOptions::new(),
        );
    }
}

impl FillCircle for CanvasRaqote {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &graphics::Color) {
        self.0.fill(
            &circle_path(x, y, r),
            &raqote::Source::Solid(into_solid_source(color)),
            &raqote::DrawOptions::new(),
        );
    }
}

fn into_solid_source(c: &graphics::Color) -> raqote::SolidSource {
    raqote::SolidSource::from_unpremultiplied_argb(c.a, c.r, c.g, c.b)
}

fn into_stroke_style(line_style: &graphics::LineStyle) -> raqote::StrokeStyle {
    raqote::StrokeStyle {
        cap: raqote::LineCap::Square,
        join: raqote::LineJoin::Miter,
        miter_limit: 0.,
        width: line_style.width as f32,
        ..Default::default()
    }
}

fn circle_path(x: f64, y: f64, r: f64) -> raqote::Path {
    let mut pb = raqote::PathBuilder::new();
    pb.arc(x as f32, y as f32, r as f32, 0., 2. * std::f32::consts::PI);
    pb.close();
    pb.finish()
}
