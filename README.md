<div align="center">

Open Source university course for computer science students,
introducing the Rust Programming Language

![](logo.png)

**[Slides](https://siscourses.ethz.ch/rust/teach-rs/slides)** •
**[Exercise Descriptions](https://siscourses.ethz.ch/rust/teach-rs/exercises)** •
**[Exercise Repo](./exercises)**

# teach-rs

</div>

Source: https://github.com/tweedegolf/teach-rs/tree/main

Selected track:
[content/rust-and-wasm.track.toml](content/rust-and-wasm.track.toml),
checked out to the root of this repo by using

```sh
git submodule init
git submodule update
cd teach-rs/modmod
cargo run -- -o ../../course -c ../../content/rust-and-wasm.track.toml --slide-url-base /rust/teach-rs/
cd -
mv course/* .
# modmod does not support images in exercises:
cp -r content/mods/alpha-rust-and-wasm/topics/embedded-wasm-apps/exercises/wasm-animation/images/ book/src/
cp content/mods/beta-cli/topics/cli/exercises/gumpiball/images/*  book/src/images
```

## Generate content

### Exercise descriptions

```sh
cargo install mdbook
cd book
mdbook build
```

### Slides

```sh
cd slides
npm install
for target in $(grep -o 'build-[^"]*' package.json) ; do
  npm run "$target"
done
```
