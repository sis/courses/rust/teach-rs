use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

/// Draw a play button on the canvas
#[wasm_bindgen]
pub fn draw_play_button() {
    alert("TODO");
}
