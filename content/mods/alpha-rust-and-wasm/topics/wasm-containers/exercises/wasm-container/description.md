# Demo / Preview: wasm container

This is a quick demo of a web container (source:
[wasmedge.org](https://wasmedge.org/docs/develop/rust/http_service/server/)). At
the moment support in container engines is experimental but already
can be tried out locally.

## Build locally

```sh
rustup target add wasm32-wasi
cargo build --target wasm32-wasi --release
```

The result can be directly tested in a wasm runtime,
e.g. [wasmedge](https://wasmedge.org/docs/start/getting-started/quick_start).

## Build the container

In the following `podman` is representative for `docker` or `buildah`
depending on your setup:

```sh
podman build --annotation "module.wasm.image/variant=compat-smart" -t wasm-server .
```

## Run the container

This needs [`crun` built with wasm support](https://wasmedge.org/docs/develop/deploy/podman/).

```sh
podman run --rm -it --annotation module.wasm.image/variant=compat-smart wasm-server
```

## k8s

If you have access to a cluster with the `crun` container runtime,
the following will declare a runtime class which can be used by pods / deployments:

```yml
---
apiVersion: node.k8s.io/v1
kind: RuntimeClass
metadata:
  name: crun
scheduling:
  nodeSelector:
    runtime: crun
handler: crun
```

After it is applied to the cluster, the following should work:

```sh
kubectl apply -f deployment.yml
```
