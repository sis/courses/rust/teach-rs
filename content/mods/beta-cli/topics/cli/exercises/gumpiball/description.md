Write a command line interface to for the prepared "gumpiball"
project.
It can run a simulation of moving 2d balls confined in a box (the
balls dont interact with each other).
It either animates the simulation in a window or saves the
trajectories of balls to a svg file:

![](images/gumpiball.svg)

Use [clap](https://docs.rs/clap/latest/clap/) to add a command line
interface to `src/main.rs`.

The interface should be able to do the following:

- [ ] Show the usage:

  ```sh
  gumpiball --help
  ```
  
   You will get this for free when using [clap](https://docs.rs/clap/latest/clap/),

- [ ] Run an animation with `n` (default: 20) balls in a window with
      dimensions `<width> x <height>` (defaults: `400.0` and `400.0`):

  ```sh
  gumpiball ui [-n <n>] [--width <width>] [--height <height>] [--paused]
  ```

  For parsed `width`, `height`, `n`, `paused` (boolean flag if omitted
  defaults to `false`), this corresponds to
  ```rs
  let animation = crate::animation::Animation::new(width, height, n);
  crate::ui::run(animation, paused)?;
  ```

- [ ] Load an animation setup from a json file, run it in memory and
      save the trajectory to a svg file:

  ```sh
  gumpiball trace -o <out> <config> [--dt <timestep>] [-n <n_snapshots>]
  ```

  For parsed `out` and `config` (json file), `dt`, `n`, this corresponds to
  ```rs
  let animation =
      crate::animation::Animation::load_from_json(&std::path::PathBuf::from(config))?;
  let trajectory = crate::animation::Trajectory::track(animation, 0.,
  n as f64 * dt, dt);
  crate::graphics::svg::save_to_svg(&trajectory, &std::path::PathBuf::from(out))?;
  ```
