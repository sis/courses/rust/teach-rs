use crate::graphics::{self, BoundingBox, Canvas, Render};

/// Moving 2D ball
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Ball {
    pub pos: [f64; 2],
    /// Velocity
    pub v: [f64; 2],
    /// Radius
    pub r: f64,
    pub color: graphics::Color,
}

/// 2D rectangle
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Rectangle {
    pub x: f64,
    pub y: f64,
    pub width: f64,
    pub height: f64,
}

/// State of the animation
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Animation {
    pub boundary: Rectangle,
    pub balls: Vec<Ball>,
}

#[derive(Debug)]
/// Record trajectories from moving balls
pub struct Trajectory {
    pub boundary: Rectangle,
    pub snapshots: Vec<Vec<Ball>>,
}

/// Orientation of a collision.
#[derive(Debug)]
enum Orientation {
    X,
    Y,
}

/// Time until x + v*t hits 0 or width.
fn t_collision_1d(x: f64, v: f64, width: f64, r: f64) -> f64 {
    if v > 0. {
        // Case: x + t*v = width - r
        (width - x - r) / v
    } else {
        // Case: x + t*v = r
        (r - x) / v
    }
}

/// Time until a collision of a ball with a containing rectangle.
fn t_collision(ball: &Ball, rectangle: &Rectangle) -> (f64, Orientation) {
    let t_x = t_collision_1d(
        ball.pos[0] - rectangle.x,
        ball.v[0],
        rectangle.width,
        ball.r,
    );
    let t_y = t_collision_1d(
        ball.pos[1] - rectangle.y,
        ball.v[1],
        rectangle.height,
        ball.r,
    );
    match (t_x >= 0., t_y >= 0.) {
        (true, true) => {
            if t_x < t_y {
                (t_x, Orientation::X)
            } else {
                (t_y, Orientation::Y)
            }
        }
        (true, false) => (t_x, Orientation::X),
        (false, true) => (t_y, Orientation::Y),
        // Orientation does not matter, t < 0 / no collision:
        (false, false) => (f64::max(t_x, t_y), Orientation::Y),
    }
}

impl Animation {
    /// Evolve the simulation by `dt`
    pub fn step(&mut self, dt: f64) {
        let mut t_remaining = dt;
        while t_remaining > 0. {
            // Determine time until next collision:
            if let Some((ball_index, (dt, orientation))) = self
                .balls
                .iter()
                .enumerate()
                .map(|(ball_index, ball)| (ball_index, t_collision(ball, &self.boundary)))
                .filter(|(_, (t, _))| 0. <= *t && *t < t_remaining)
                .min_by(|(_, (t1, _)), (_, (t2, _))| {
                    t1.partial_cmp(t2).unwrap_or(std::cmp::Ordering::Equal)
                })
            {
                self.substep(dt);
                t_remaining -= dt;
                let dim = match orientation {
                    Orientation::X => 0,
                    Orientation::Y => 1,
                };
                let ball = &mut self.balls[ball_index];
                ball.v[dim] = -ball.v[dim];
            } else {
                self.substep(t_remaining);
                break;
            }
        }
    }

    /// Evolve the simulation by `dt` without collisions
    pub fn substep(&mut self, dt: f64) {
        for ball in &mut self.balls {
            ball.pos[0] += ball.v[0] * dt;
            ball.pos[1] += ball.v[1] * dt;
        }
    }

    /// Load an animation from a json file
    pub fn load_from_json(path: &std::path::Path) -> Result<Self, Box<dyn std::error::Error>> {
        let file = std::fs::File::open(path)?;
        let reader = std::io::BufReader::new(file);
        Ok(serde_json::from_reader(reader)?)
    }

    /// Create a new animation in a box with specified width and height
    /// and `n` randomly generated balls
    pub fn new(width: f64, height: f64, n: usize) -> Self {
        let random_ball = || {
            const MIN_R: f64 = 2.;
            const MAX_R: f64 = 50.;
            const MAX_V: f64 = 1.;
            let r = MIN_R + (MAX_R - MIN_R) * rand::random::<f64>();
            Ball {
                pos: [
                    r + rand::random::<f64>() * (width - 2. * r),
                    r + rand::random::<f64>() * (height - 2. * r),
                ],
                v: [
                    (2. * rand::random::<f64>() - 1.) * MAX_V,
                    (2. * rand::random::<f64>() - 1.) * MAX_V,
                ],
                r,
                color: graphics::Color {
                    r: rand::random::<u8>(),
                    g: rand::random::<u8>(),
                    b: rand::random::<u8>(),
                    a: 255,
                },
            }
        };

        Self {
            boundary: Rectangle {
                x: 0.,
                y: 0.,
                width,
                height,
            },
            balls: (0..n).map(|_| random_ball()).collect(),
        }
    }
}

impl Trajectory {
    /// Create an empty trajectory
    pub fn new(boundary: Rectangle) -> Self {
        Self {
            boundary,
            snapshots: Vec::new(),
        }
    }
    /// Track trajectories from an animation
    pub fn track(mut animation: Animation, t_start: f64, t_stop: f64, dt: f64) -> Self {
        animation.step(t_start);
        let mut t = t_start;
        let mut trajectory = Self::new(animation.boundary.clone());
        while t < t_stop {
            trajectory.take_snapshot(&animation.balls);
            animation.step(dt);
            t += dt;
        }
        trajectory
    }
    /// Take a snapshot, removing objects in the previous snapshot if they did not move.
    pub fn take_snapshot(&mut self, balls: &[Ball]) {
        if let Some(snapshot) = self.snapshots.last_mut() {
            let remove_indices = snapshot
                .iter()
                .enumerate()
                .rev()
                .filter_map(|(i, obj)| {
                    if obj.v[0] == 0. && obj.v[1] == 0. {
                        Some(i)
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();
            for i in remove_indices {
                snapshot.remove(i);
            }
        }
        self.snapshots.push(balls.to_vec());
    }
}

/// Helper struct to override the opacity of an object when rendering
struct WithAlpha<T> {
    object: T,
    alpha: f64,
}

impl Render for WithAlpha<&[Ball]> {
    fn render(&self, canvas: &mut impl Canvas) {
        for ball in self.object {
            let graphics::Color { r, g, b, a } = &ball.color;
            canvas.fill_circle(
                ball.pos[0],
                ball.pos[1],
                ball.r,
                &graphics::Color {
                    r: *r,
                    g: *g,
                    b: *b,
                    a: ((*a as f64 / 255. * self.alpha) * 255.0) as u8,
                },
            );
        }
    }
}

impl Render for Animation {
    fn render(&self, canvas: &mut impl Canvas) {
        canvas.fill_rectangle(
            self.boundary.x,
            self.boundary.y,
            self.boundary.width,
            self.boundary.height,
            &graphics::Color::WHITE,
        );
        canvas.stroke_rectangle(
            self.boundary.x,
            self.boundary.y,
            self.boundary.width,
            self.boundary.height,
            &graphics::LineStyle::DEFAULT_LINE_STYLE,
        );
        WithAlpha {
            object: self.balls.as_slice(),
            alpha: 1.0,
        }
        .render(canvas);
    }
}

impl Render for Trajectory {
    fn render(&self, canvas: &mut impl Canvas) {
        const ALPHA_MULTIPLIER: f64 = 0.9;
        let alphas: Vec<f64> = std::iter::successors(Some(1.0), |a| Some(a * ALPHA_MULTIPLIER))
            .take(self.snapshots.len())
            .collect();
        for (alpha, snapshot) in alphas.into_iter().rev().zip(&self.snapshots) {
            WithAlpha::<&[Ball]> {
                object: snapshot,
                alpha,
            }
            .render(canvas);
        }
    }
}

impl BoundingBox for Trajectory {
    fn bounding_box(&self) -> crate::animation::Rectangle {
        self.boundary.clone()
    }
}
